#include <stdio.h>
#include <iostream>
#include <string.h>
#include <algorithm>
#include <functional>
#include <math.h>

int main(){

  float A[] = {92041.516, -141.490, 4005.671, 52769.992, 1040600.812};
  //1, 2, 3, 0, 4
  int    B[] = { 1, 2, 3, 4, 5 };

  int sizes = 5;

  std::vector<double> a(sizes);
  for (int i = 0; i < sizes; i++){
    a[i] = A[i];
  }

  std::sort(a.begin(), a.end());

  std::cout << "Correctly ranking: ";
  for (int i = 0; i < sizes; i++){
    std::cout << a[i] <<  " ";
  }
  std::cout << std::endl;

  std::vector<unsigned> p(sizes);
  std::iota(p.begin(), p.end(), 0);
  std::sort(p.begin(), p.end(), [&](unsigned i1, unsigned i2) { return A[i1] < A[i2]; });

  std::cout << "Sorted values: " << std::endl;

  for(int i = 0; i < sizes; i++){
    std::cout << " index: " << p[i] << " value: " << A[p[i]] <<  std::endl;
  }

  /*
  //sort ok
  std::sort(A, A + sizes);
  for(int i = 0; i < sizes; i++){
    std::cout << A[i] << " ";
  }
  std::cout << std::endl;*/

  return 0;
}
