#include <stdio.h>

#include <cuda.h>
#include <curand_kernel.h>

#include <thrust/device_ptr.h>
#include <thrust/device_malloc.h>
#include <thrust/device_free.h>
#include <thrust/reduce.h>
#include <thrust/sequence.h>
#include <thrust/sort.h>


#define NTHREADS_POPULATION 256
#define BUCKET_POPULATION     1

inline void checkError(cudaError result)
{
  if (result!=cudaSuccess)
    {
      printf("Cuda error!!!\n");
      exit(1);
    }
}

inline void checkErrorAlloc(cudaError result)
{
  if (result!=cudaSuccess)
    {
      printf("GPU memory allocation error!!!\n");
      exit(1);
    }
}

int iDivUp(int a,int b)
{
  return ( (a%b)? (a/b)+1 :a/b );
}

__device__  unsigned computePosImproved(unsigned indiv,unsigned *order)
{
  if (order==NULL)
    return indiv;
  else
    return order[indiv];
}

__device__  unsigned computePos(unsigned indiv,unsigned dim,unsigned size,unsigned ndim,unsigned *order)
{
  if (order==NULL)
    return (indiv*ndim)+dim;
  else
    return (order[indiv]*ndim)+dim;
}

__global__ void findFirstNonImprovedPosKernel(int *improved,unsigned size,unsigned *order,unsigned *result)
{
  unsigned threadIndex = (blockIdx.x*blockDim.x) + threadIdx.x;

  unsigned posImproved;

  if (threadIndex<size){
      posImproved = computePosImproved(threadIndex,order); //Creo que usar order es obligatorio. No quitar

      result[threadIndex]=(improved[posImproved]==0) ? threadIndex : size;
  }
}

unsigned findFirstNonImprovedPos(int *improved,unsigned size,unsigned *order)
{
  unsigned *result;
  cudaError cError = cudaMalloc((void **)(&result),	size * sizeof(unsigned));
  checkErrorAlloc(cError);

  findFirstNonImprovedPosKernel<<< iDivUp(size, NTHREADS_POPULATION), NTHREADS_POPULATION>>>(improved,size,order,result);

  thrust::device_ptr<unsigned> dev_ptr_result = thrust::device_pointer_cast(result);
  unsigned pos = thrust::reduce(dev_ptr_result, dev_ptr_result+size, std::numeric_limits<unsigned>::max(), thrust::minimum<unsigned>());

  cError = cudaFree(result);
  checkError(cError);

  return pos;

}


void sortPopulationByFitness(float *individuals,float *fitness, unsigned *order, unsigned size,unsigned ndim)
{
  float *tmpFitness;
  cudaError cError;

  //Create a copy of the fitness vector
  //Don't want the fitness vector to get sorted
  unsigned tmpSize = size * sizeof(float);

  cError = cudaMalloc((void **)(&tmpFitness),tmpSize);
  checkErrorAlloc(cError);
  cudaMemcpy(tmpFitness,fitness,tmpSize,cudaMemcpyDeviceToDevice);
  checkError(cError);

  // wrap raw pointers with a device_ptr
  thrust::device_ptr<float> dev_ptr_fitness  = thrust::device_pointer_cast(tmpFitness);
  thrust::device_ptr<unsigned> dev_ptr_order = thrust::device_pointer_cast(order);

  //Init the order vector to a 0,1,2,3... sequence
  thrust::sequence(dev_ptr_order, dev_ptr_order + size, 0, 1);

  //sort indices by fitness
  thrust::sort_by_key(dev_ptr_fitness,dev_ptr_fitness + size, dev_ptr_order);

  cError = cudaFree(tmpFitness);
  checkError(cError);
}

__global__ void initPopulationKernel(unsigned indiv,float *individuals,
									float *fitness,int *improved, 
									unsigned *order,unsigned size,
									unsigned ndim, curandState *devStates)
{
	unsigned threadIndex = (blockIdx.x * blockDim.x) + threadIdx.x;
	unsigned start = threadIndex*BUCKET_POPULATION;
	unsigned end = thrust::min(start + BUCKET_POPULATION, ndim);

	float randomNumber;
	unsigned pos;
	//range = bounds.max - bouns.min
	float range = 10.0 + 10.0;

	curandState state = devStates[threadIndex];

	for (unsigned i = start; i < end; i++){
		randomNumber = curand_uniform(&state);
		pos = computePos(indiv, i, size, ndim, NULL);
		//-10 = bounds.min
		individuals[pos] = (randomNumber * range)+ -10.0;
		//printf("%d pos[%d] = %.2f\n", indiv, pos, individuals[pos]);
	}

	//These parameters are represented by one value per individual
	if (threadIndex==0){
		order[indiv] = indiv;
		fitness[indiv] = (randomNumber * range)+ -10.0;
		printf("fitness[%d] = %.2f\n", indiv, fitness[indiv]);
		improved[indiv]=0;
	}
	devStates[threadIndex] = state;
}


void randomInitPop(float *individuals,float *fitness, int *improved, unsigned *order,unsigned size,unsigned ndim, curandState *devStates){
	dim3 nBlocks(iDivUp(ndim,NTHREADS_POPULATION*BUCKET_POPULATION),1,1);
	dim3 nThreads(NTHREADS_POPULATION,1,1);
	
	srand (time(NULL));
  	unsigned seed=rand();
	
	for (unsigned indiv=0;indiv<size;indiv++)
		initPopulationKernel<<<nBlocks,nThreads>>>(indiv,individuals,fitness,improved,order,size,ndim, devStates);
}

__global__ void initRandomNumbersKernel(curandState *state )
{
  unsigned index = (blockIdx.x*blockDim.x) + threadIdx.x;
  curand_init(1267,index,0,&state[index]);

}


int main(){
	cudaError cError;

	curandState *devStates;

	cError = cudaMalloc( (void **)&devStates, 256 * sizeof(curandState));
	checkErrorAlloc(cError);

	initRandomNumbersKernel<<< 8, 32 >>>(devStates);
	
	float *individuals;
	float *fitness;
	int *improved;
	unsigned *order;

	unsigned size = 10;
	unsigned ndim = 10;
	
	
	unsigned individualsSize = size * ndim;
	
	cError = cudaMalloc((void **)(&individuals), individualsSize * sizeof(float));
	cError = cudaMalloc((void **)(&improved), size * sizeof(int));
	cError = cudaMalloc((void **)(&order), size*sizeof(unsigned));
	cError = cudaMalloc((void **)(&fitness),	size*sizeof(float));

	checkErrorAlloc(cError);
	randomInitPop(individuals, fitness, improved, order, size, ndim, devStates);
	printf("findFirstNonImproved: %u\n", findFirstNonImprovedPos(improved,size,order));

	sortPopulationByFitness(individuals, fitness, order, size, ndim);

	unsigned *h_order = new unsigned[size];
	cudaMemcpy(h_order, order, size * sizeof(unsigned), cudaMemcpyDeviceToHost);

	for(int i = 0; i < size; i++)
		printf("%u ", h_order[i]);

	printf("findFirstNonImproved: %u\n", findFirstNonImprovedPos(improved,size,order));
	
	thrust::device_ptr<float> dev_ptr_fitness = thrust::device_pointer_cast(fitness);
	float pos = thrust::reduce(dev_ptr_fitness, dev_ptr_fitness+size, std::numeric_limits<float>::max(), thrust::minimum<float>());
	printf("menor value é: %.2lf\n", pos);
	return 0;
}
