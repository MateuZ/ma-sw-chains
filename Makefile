OBJ_PATH = objects
SRC_PATH = source

SRCS = $(wildcard $(SRC_PATH)/*.cpp)

OBJS = $(patsubst %.cpp, $(OBJ_PATH)/%.o, $(notdir $(SRCS)))

CC = g++ -std=c++17 -O3

BOOST_FLAG = -lboost_program_options

all: ma-sw-chains

ma-sw-chains: $(OBJS)
	$(CC) $^ $(BOOST_FLAG) -o $@

$(OBJ_PATH)/%.o : $(SRC_PATH)/%.cpp
	$(CC) -o $@ -c $<

clean:
	-rm -f $(OBJ_PATH)/*.o ma-sw-chains

run:
	./ma-sw-chains
