/*
 * Copyright (C) 2013 Miguel Lastra
 *
 * This file is part of GPU-MA-SW-CHAINS.
 *
 * GPU-MA-SW-CHAINS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * GPU-MA-SW-CHAINS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GPU-MA-SW-CHAINS.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SolisWets.h"
//#include "SolisWets.hcu"
#include "Population.h"
#include <assert.h>

SolisWets::SolisWets(unsigned _size,unsigned _ndim,domainBounds bounds,SolisWetsParameters _soliswetsparam,Fitness *_fitness)
{
  size = _size;
  ndim = _ndim;
  soliswetsparam = _soliswetsparam;
  fitness = _fitness;
  improvedPopulation = NULL;

  improvedPopulation = new Population( 1, ndim, bounds, false);
}

void SolisWets::changeIndividual(unsigned indiv,float *individuals,float *improvedIndividuals,
				      float *step,float *bias,unsigned size,unsigned ndim,
              domainBounds bounds,unsigned *order,float *diff,int direction
){
  std::mt19937 rng;
  rng.seed(std::random_device{}());
  //mean = 0.00 stddev 1.00
  std::normal_distribution<float> random(0.00, 1.00);

  unsigned posImproved, posOriginal;
  unsigned posBias, posDelta;

  //faz a perturbação nas dimensões;
  for( int dim = 0; dim < ndim; dim++ ){
    posImproved = computePos(0, dim, 1, ndim, NULL);
    posOriginal = computePos(indiv, dim, size, ndim, order);
    posBias = computePosBias(indiv, dim, size, ndim, order);

    if( direction == 0 ){
      posDelta = computePosDelta(indiv, order);
      float delta = step[posDelta];
      diff[dim] = ( random(rng) * delta );
      improvedIndividuals[posImproved] = individuals[posOriginal] + bias[posBias] + diff[dim];
    }else{
      improvedIndividuals[posImproved] = individuals[posOriginal] - bias[posBias] - diff[dim];
    }

    if( bounds.active ){
      improvedIndividuals[posImproved] = std::max(bounds.min,improvedIndividuals[posImproved]);
      improvedIndividuals[posImproved] = std::min(bounds.max,improvedIndividuals[posImproved]);
    }
  }
}

int isBetter(unsigned indiv,float *individualsFitness,
  float *improvedIndividualsFitness, unsigned size,
  unsigned ndim, unsigned *order,float &improvement
){
  unsigned posFitness = order[indiv];
  unsigned posImprovedFitness = 0;

  float diff = individualsFitness[posFitness]-improvedIndividualsFitness[posImprovedFitness];

  if( diff > 0.0 )
    improvement += diff;

  return (diff > 0.0)? 1 : 0;
}


void incrementBiasKernel(unsigned indiv,float *individualsBias,
				    unsigned size,unsigned ndim,unsigned *order,float *diff)
{
  for( int dim = 0; dim < ndim; dim++ ){
    unsigned posBias = computePosBias(indiv,dim,size,ndim,order);
    individualsBias[posBias]= 0.2 * individualsBias[posBias] + 0.4 * ( diff[dim] + individualsBias[posBias]);
  }
}

void decrementBiasKernel(unsigned indiv,float *individualsBias,
				unsigned size,unsigned ndim,unsigned *order,float *diff)
{
  for( int dim = 0; dim < ndim; dim++ ){
    unsigned posBias = computePosBias(indiv,dim,size,ndim,order);
    individualsBias[posBias] = individualsBias[posBias] - 0.4 * ( diff[dim] + individualsBias[posBias]);
  }
}

void substituteIndividual(
  unsigned index0, unsigned index1,
  float *individuals, float *improvedIndividuals,
  float *individualsFitness,float *improvedIndividualsFitness,
  float *individualsStep,float *individualsBias,
  int *individualsImproved, unsigned sizeIndividuals,
  unsigned sizeImprovedIndividuals,unsigned ndim,
  unsigned *order,int fullInit,float delta
){
  unsigned posA, posB, posBias;
  for( int dim = 0; dim < ndim; dim++ ){
    posA = computePos(index0, dim, sizeIndividuals, ndim, order);
    posB = computePos(index1, dim, sizeImprovedIndividuals, ndim, NULL);
    individuals[posA] = improvedIndividuals[posB];

    if( fullInit > 0){
      posBias = computePosBias(index0, dim, sizeIndividuals, ndim, order);
      individualsBias[posBias] = 0.0;
    }
  }

  unsigned posFitness = computePosFitness(index0, order);
  individualsFitness[posFitness] = improvedIndividualsFitness[index1];

  if( fullInit > 0){
    unsigned posImproved = computePosImproved(index0, order);
    individualsImproved[posImproved] = 0;
  }

  unsigned posStep = computePosDelta(index0, order);
  individualsStep[posStep] = delta;
}

void SolisWets::search(Population *population,unsigned nEval)
{

  /* Population individuals */
  unsigned ndim = population->getDimensions();
  unsigned size = population->getSize();
  unsigned *order = population->getOrder();
  int *ptr_numSuccess = population->getNumSuccess();
  int *ptr_numFailed  = population->getNumFailed();
  float *individuals = population->getIndividuals();
  float *individualsFitness = population->getFitness();
  domainBounds bounds = population->getBounds();
  int *individualsImproved = population->getImproved();

  float *improvedIndividuals = improvedPopulation->getIndividuals();
  float *step = population->getStep();
  float *bias = population->getBias();

  float *improvedIndividualsFitness = improvedPopulation->getFitness();
  float *shift = fitness->getShift();
  float fbias = fitness->getBias();
  func fitnessFunction = fitness->getFunction();

  /*improvePopulation(population->getIndividuals(),improvedPopulation->getIndividuals(),
		    population->getStep(),population->getBias(),
		    population->getFitness(),improvedPopulation->getFitness(),
		    population->getImproved(),population->getNumSuccess(),population->getNumFailed(),
		    nEval,soliswetsparam,fitness->getFunction(),
		    fitness->getShift(),fitness->getBias(),
		    size,ndim,population->getBounds(),population->getOrder());
	*/

  bool copy = false;
  float improvement = 0.0;
  float *diff = new float[ndim];

  unsigned indiv = findFirstNonImprovedPos(population->getImproved(), size, population->getOrder());

  if( indiv > (size - 1) ){
    std::cout << "No one to improve! Randomly choosing an individual\n";
    indiv = ((long)rand()/( (long) RAND_MAX + 1))*size;
  }

  unsigned posNum = order[indiv];
  std::cout << "Indiv: " << indiv << " Improving: " << posNum << std::endl;

  int numSuccess = ptr_numSuccess[posNum];
  int numFailed  = ptr_numFailed[posNum];

  for( unsigned e = 0; e < nEval; e++ ){
    changeIndividual(indiv, individuals, improvedIndividuals, step, bias,
      size, ndim, bounds, order, diff, 0
    );

    fitnessFunction(improvedIndividuals,improvedIndividualsFitness,1,ndim,shift,fbias,0,NULL);

    if( isBetter(indiv,individualsFitness,improvedIndividualsFitness,size,ndim, order,improvement) ){
      copy = true;

      incrementBiasKernel(indiv,bias,size,ndim,order,diff);

      numSuccess++;
      numFailed = 0;

    } else {

      changeIndividual(indiv, individuals, improvedIndividuals, step, bias,
        size, ndim, bounds, order, diff, 1
      );

      fitnessFunction(improvedIndividuals,improvedIndividualsFitness,1,ndim,shift,fbias,0,NULL);

      e++;

      if( isBetter(indiv,individualsFitness,improvedIndividualsFitness,size,ndim,order,improvement ) ){
        copy = true;

        decrementBiasKernel(indiv,bias,size,ndim,order,diff);

        numSuccess++;
        numFailed = 0;
      } else {
        numFailed++;
        numSuccess = 0;
      }
    }
    if( copy ){
      copy = false;
      substituteIndividual(indiv,0,individuals,improvedIndividuals,
							     individualsFitness,improvedIndividualsFitness,
							     step,bias,individualsImproved,size,1,ndim,order,
                   0,soliswetsparam.delta);
    }

    unsigned posDelta = order[indiv];
    if( numSuccess >= soliswetsparam.maxSuccess ){
      step[posDelta] *= 2.0;
      numSuccess = 0;
    } else if( numFailed > soliswetsparam.maxFailed ) {
      step[posDelta] /= 2.0;
      numFailed = 0;
    }
  }

  unsigned posImproved = order[indiv];
  std::cout << "Improvement of " << indiv << " = " << improvement << std::endl;
  if (improvement < 1e-8){
      std::cout << "Marking for not improving: " << indiv << ".I=" << improvement << std::endl;
      individualsImproved[posImproved] = 1;
    }

  ptr_numSuccess[posNum]= numSuccess;
  ptr_numFailed[posNum] = numFailed;

  free(diff);
}

SolisWets::~SolisWets()
{
  delete improvedPopulation;
}
