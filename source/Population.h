/*
 * Copyright (C) 2013 Miguel Lastra
 *
 * This file is part of GPU-MA-SW-CHAINS.
 *
 * GPU-MA-SW-CHAINS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * GPU-MA-SW-CHAINS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GPU-MA-SW-CHAINS.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _POPULATION_H
#define _POPULATION_H

//#include "Global.hcu" //contem alguns define e funcoes pra gpu

#include "Global.h"

class Population
{
  private:
    unsigned size; //tamanho da populacao
    unsigned ndim; //num de dimensoes de cada individuo
    float *individuals; //um vetorzão com os individuos
    float *fitness; //um vetor que guarda o fitness de cada individuo
    unsigned *order; //stores the indices of the individuals sorted by fitness

    //SW related
    float *step; //stores the final step of each individual
    float *bias; //stores the bias associated to each dimension of each individual
    int *improved; //stores, for each individual if it has already been improved without +1e-8 success

    int *numSuccess; //SW
    int *numFailed;  //SW

    bool minimal; //stores if it is a full blown population or just a temporary object

    domainBounds bounds;

  public:
    Population();
    Population(unsigned _size,unsigned _ndim,domainBounds _bounds,bool init=true);
    void minimalInit(unsigned _size,unsigned _ndim);
    unsigned getSize();
    unsigned getDimensions();
    float *getIndividuals();
    float *getFitness();
    float *getStep();
    float *getBias();
    int	*getImproved();
    int	*getNumSuccess();
    int	*getNumFailed();
    unsigned *getOrder();
    void sortByFitness();
    ~Population();
    void print(int verbose=0);
    void calcDistance(unsigned parent, unsigned candidate);
    domainBounds getBounds();
    double distanceToOptimum(float *shift);
    float getBestFitness();
    float getBestIndividualStatistics(float &variance);
    double calcInertia();
};

inline unsigned Population::getSize()
{
  return size;
}

inline unsigned Population::getDimensions()
{
  return ndim;
}

inline float *Population::getIndividuals()
{
  return individuals;
}

inline float *Population::getFitness()
{
  return fitness;
}

inline float *Population::getStep()
{
  return step;
}

inline float *Population::getBias()
{
  return bias;
}

inline unsigned *Population::getOrder()
{
  return order;
}

inline domainBounds Population::getBounds()
{
  return bounds;
}

inline int *Population::getImproved()
{
  return improved;
}

inline int *Population::getNumSuccess()
{
  return numSuccess;
}

inline int *Population::getNumFailed()
{
  return numFailed;
}

#endif
