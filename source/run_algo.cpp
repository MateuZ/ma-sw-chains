#include "run_algo.hpp"
#include "Population.h"
#include "Fitness.h"
#include "RosenbrockFitness.h"
#include "Cross.h"
#include "SolisWets.h"

#include <boost/date_time/posix_time/posix_time.hpp>

std::tuple<double, double, double> run_algorithm(
  uint pop_size, uint n_evals, uint n_dim,
  std::string FuncObj, std::string output_file,
  bool conv_plot
){
  std::ofstream output;

  if( conv_plot ){
    output.open(output_file);
    if( not output.is_open() ){
      std::cerr << "/* FAILED OPENING THE OUTPUT FILE! */" << '\n';
      exit(1);
    }
  }

	v_tuple_3d stats;

  domainBounds bounds;
  bounds.min = -10.0;
  bounds.max = +10.0;
  bounds.active = true;
  bounds.delta = 0.02 * ( (bounds.active)? ( bounds.max - bounds.min ) : 1);

  //shift function to -330.0;
  float *o = new float[n_dim];
  float fbias = -330.0;
  float middle = (bounds.max - bounds.min) / 2.0 + bounds.min;

  for( unsigned i = 0; i < n_dim; i++ )
    o[i] = middle + 0.1;

  Fitness *f;
  if( FuncObj == "shifted_rosenbrock" ){
      f = new RosenbrockFitness(o, fbias, n_dim);
  } else if ( FuncObj == "shifted_griewank" ){

  } else {
    std::cerr << "Wrong funtion type" << std::endl;
    exit(1);
  }

  CrossBLX *c = new CrossBLX( pop_size, n_dim, f );

  printf("WARNING: population size was increased to crossover step.\n");
  pop_size += 20; // PARALLEL_CROSS = 20

  Population *p = new Population( pop_size, n_dim, bounds );

  std::cout << "Initial evaluating of population..." << std::endl;
  f->eval(p);

  std::cout << "Sorting population..." << std::endl;
  p->sortByFitness();

  std::cout<< "Best initial fitness value: "<< p->getBestFitness() <<std::endl;

	unsigned alternate 	= 500;
  unsigned nEval      = alternate;
	unsigned iterations = (n_evals / alternate) / 2;
	double alpha		    = 0.5;

  /* Solis Wets algorithm */
  SolisWetsParameters soliswetsparam = { 5, 3, bounds.delta };

  SolisWets *s = new SolisWets( pop_size, n_dim, bounds, soliswetsparam, f );

	/* number of candidates to BLX */
	unsigned nCandidates = 3;

  boost::posix_time::ptime prog_tick, prog_now;
  boost::posix_time::time_duration  prog_diff;

  prog_tick = boost::posix_time::microsec_clock::local_time();


  for( int it = 1; it <= iterations; it++ ){

    if( output.is_open() )
      output << p->getBestFitness() << " " << p->calcInertia() << std::endl;

    std::cerr << "Crossing step: " << it << std::endl;
    c->cross( p, nEval, nCandidates, alpha );
		std::cerr << "Best fitness value: " << p->getBestFitness() << std::endl;

    if( output.is_open() )
      output << p->getBestFitness() << " " << p->calcInertia() << std::endl;


		std::cerr << "Solis Wets step: " << it << std::endl;
    s->search( p, nEval );


    std::cout << "Sorting population..." << std::endl;
    p->sortByFitness();
		std::cerr << "Best fitness value: " << p->getBestFitness() << std::endl;
	}

  prog_now = boost::posix_time::microsec_clock::local_time();
  prog_diff = prog_now - prog_tick;

  if( output.is_open() ){
    output.close();
  }
	return std::make_tuple(static_cast<double>(p->getBestFitness()), static_cast<double>(prog_diff.total_milliseconds() / 1000.0), p->distanceToOptimum(o) );
}
