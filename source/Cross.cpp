/*
 * Copyright (C) 2013 Miguel Lastra
 *
 * This file is part of GPU-MA-SW-CHAINS.
 *
 * GPU-MA-SW-CHAINS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * GPU-MA-SW-CHAINS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GPU-MA-SW-CHAINS.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Cross.h"
//#include "Cross.hcu"
#include <assert.h>
#include <cstdlib>
#include <time.h>

CrossBLX::CrossBLX(unsigned _size, unsigned _ndim, Fitness *_fitness){
  size    = _size;
  ndim    = _ndim;
  fitness = _fitness;

  generatedPopulation = NULL;
  generatedPopulation = new Population();
  generatedPopulation->minimalInit( 1, ndim );
}

void CrossBLX::cross(Population *population, unsigned nEval, unsigned nCandidates, float alpha){

  unsigned ndim = population->getDimensions();
  unsigned size = population->getSize();

  unsigned merged = 0;

  //PARALLEL_CROSS = 20

  //generates 20 new individuals each call;
  unsigned PARALLEL_CROSS = 20;
  for( unsigned i = 0; i < ( nEval / PARALLEL_CROSS ); i++ ){

      crossPopulation(population->getIndividuals(), population->getImproved(),
                      nCandidates,alpha,size,ndim, population->getBounds(),
                      population->getOrder());

      fitness->eval( population, size - PARALLEL_CROSS );

      population->sortByFitness();
  }
}

void CrossBLX::crossPopulation(float *individuals, int *improved, unsigned nCandidates, float alpha, unsigned size, unsigned ndim, domainBounds bounds, unsigned *order){

  std::mt19937 rng;
  rng.seed(std::random_device{}());
  std::uniform_real_distribution<float> random(0.00, 1.00);

  //the cicle will generates 20 new offsprings
  unsigned PARALLEL_CROSS = 20;

  //each new individual has 2 parents;
  unsigned parents[ 2 * PARALLEL_CROSS ];
  unsigned candidate;

  unsigned usableSize = size - PARALLEL_CROSS;

  float dist, maxdist;

  maxdist = -1.0;
  srand(time(NULL));
  
  for( unsigned indiv = 0; indiv < PARALLEL_CROSS; indiv++ ){
      maxdist = -1.0;

      //get the first parent randomly
      parents[indiv] = rand() % usableSize;

      if( parents[indiv] >= usableSize ){
        printf("Error num aleatorio: %d",parents[indiv]);
        exit(1);
      }

      //select the most dissimilar match between the first parent and nCandidates
      for( unsigned i = 0; i < nCandidates; i++ ){
        candidate = rand() % usableSize;

		if( candidate >= usableSize ){
			printf("Error num aleatorio: %d", candidate);
			exit(1);
		}

		dist = distance(individuals, parents[indiv], candidate, size, ndim, order);

		if( ( dist <= 0) && ( parents[indiv] != candidate) ){
			printf("Error selecting parents %f\n",dist);
			printf("Mom: %d Candidate: %d\n", parents[indiv], candidate);
			exit(1);
		}

		if( dist > maxdist ){
			maxdist = dist;
			parents[indiv + PARALLEL_CROSS] = candidate;
		}
      }

    while( parents[indiv] == parents[indiv + PARALLEL_CROSS] ){
      fprintf(stderr, "Hermaphrodite detected. Fixing...\n");
      parents[indiv + PARALLEL_CROSS] = (parents[indiv + PARALLEL_CROSS] + 1) % usableSize;
    }

    //crossover

    /*printf("Mom: %-3d Dad: %-3d Child: %-3d\n",
      parents[indiv],
      parents[indiv + PARALLEL_CROSS],
      (size - PARALLEL_CROSS) + indiv
    );*/

    unsigned posMom, posDad, posChild, posImproved;
    float value0, value1, I, A1, B1, tmp;

    //always will get the ends position;
    posImproved = computePosImproved((size - PARALLEL_CROSS) + indiv, order);
    improved[posImproved] = 0;

    for( unsigned d = 0; d < ndim; d++ ){
      posMom = computePos(parents[indiv], d, size, ndim, order);
      posDad = computePos(parents[indiv + PARALLEL_CROSS], d, size, ndim, order);

      value0 = individuals[posMom];
      value1 = individuals[posDad];

      tmp = value0;
      value0 = std::min( value0, value1 );
      value1 = std::max( tmp, value1 );

      I = (value1 - value0) * alpha;
      A1 = value0 - I;
      B1 = value1 + I;

      if( bounds.active ){
        A1 = std::max( bounds.min, A1 );
        B1 = std::min( bounds.max, B1 );
      }

      //Put the new child in the extra space
      posChild = computePos( (size - PARALLEL_CROSS) + indiv, d, size, ndim, order);
      //This ensure a random new value between [A1, B1]
      individuals[posChild] = A1 + random(rng) * (B1 - A1);

      //printf("New value: %.3f \n", individuals[posChild]);
    }
  }
}

CrossBLX::~CrossBLX(){
  delete generatedPopulation;
}
