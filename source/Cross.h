/*
 * Copyright (C) 2013 Miguel Lastra
 *
 * This file is part of GPU-MA-SW-CHAINS.
 *
 * GPU-MA-SW-CHAINS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * GPU-MA-SW-CHAINS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GPU-MA-SW-CHAINS.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _CROSS_H
#define _CROSS_H

#include "Population.h"
#include "Fitness.h"
#include "Global.h"

class CrossBLX
{
  private:
    unsigned size;
    unsigned ndim;

    Population *generatedPopulation;
    Fitness *fitness;

  public:
    CrossBLX(unsigned _size,unsigned _ndim,Fitness *_fitness);
    void cross(Population *population,unsigned nEval,unsigned nCandidates,float alpha);
    void crossPopulation(float *individuals, int *improved, unsigned nCandidates,
      float alpha, unsigned size, unsigned ndim, domainBounds bounds, unsigned *order);
    ~CrossBLX();
};


#endif
