#include "Global.h"
#include <cstddef>

unsigned computePos(unsigned indiv,unsigned dim,unsigned size,unsigned ndim,unsigned *order)
{
  if( order == NULL )
    return (indiv * ndim) + dim;
  else
    return (order[ indiv ] * ndim) + dim;
}

unsigned computePosFitness(unsigned indiv,unsigned *order)
{
  if (order==NULL)
    return indiv;
  else
    return order[indiv];
}

unsigned computePosImproved(unsigned indiv,unsigned *order)
{
  if( order == NULL )
    return indiv;
  else
    return order[indiv];
}

unsigned computePosBias(unsigned indiv,unsigned dim,unsigned size,unsigned ndim,unsigned *order)
{
  if( order == NULL )
    return (indiv * ndim) + dim;
  else
    return (order[indiv] * ndim) + dim;
}

unsigned computePosDelta(unsigned indiv,unsigned *order)
{
  if( order == NULL )
    return indiv;
  else
    return order[indiv];
}

unsigned findFirstNonImprovedPos(int *improved, unsigned size, unsigned *order){
  unsigned posImproved;

  unsigned *result = new unsigned[size];

  for( int indiv = 0; indiv < size; indiv++ ){
    posImproved = computePosImproved( indiv, order );
    result[indiv] = (improved[posImproved] == 0)? indiv : size;
	//printf("%u ", result[indiv]); 
  }
  //printf("\n");
  unsigned pos = *std::min_element(result, result + size);
  //printf("pos: %u, best: %u\n", pos, order[0]);
  free(result);
  return pos;
}
/*
 * Calculates the euclidian distance between the parent and the candidate;
*/
double distance(float *individuals, unsigned parent, unsigned candidate, unsigned size, unsigned dim, unsigned *order){

  double accumDiff = 0.0, diff;
  unsigned pos0, pos1;

  for( unsigned d = 0; d < dim; d++ ){
    pos0 = computePos(parent, d, size, dim, order);
    pos1 = computePos(candidate, d, size, dim, order);

    diff = individuals[pos0] - individuals[pos1];
    accumDiff += (diff * diff);
  }

  return sqrt(accumDiff);
}
