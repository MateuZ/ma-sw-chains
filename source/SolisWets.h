/*
 * Copyright (C) 2013 Miguel Lastra
 *
 * This file is part of GPU-MA-SW-CHAINS.
 *
 * GPU-MA-SW-CHAINS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * GPU-MA-SW-CHAINS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GPU-MA-SW-CHAINS.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _SOLISWETS_H
#define _SOLISWETS_H

#include "Population.h"
#include "Fitness.h"

typedef struct
{
  unsigned maxSuccess;
  unsigned maxFailed;
  float delta;
} SolisWetsParameters;

class SolisWets
{
  private:
    unsigned size;
    unsigned ndim;
    SolisWetsParameters soliswetsparam;

    Population *improvedPopulation;
    Fitness *fitness;

  public:
    SolisWets(unsigned _size,unsigned _ndim,domainBounds bounds,SolisWetsParameters _soliswetsparam,Fitness *_fitness);
    void changeIndividual(unsigned indiv,float *individuals,float *improvedIndividuals,
    				      float *step,float *bias,unsigned size,unsigned ndim,
                  domainBounds bounds,unsigned *order,float *diff,int direction);
    void search(Population *population,unsigned nEval);
    ~SolisWets();
};

#endif
