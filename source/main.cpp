/*
 * Step-by-step of the ma-sw-chains algorithm
 * -------------------------------------------------------
 * 1.  Generate the initial population
 * 2.  Perform the steady-state GA throughut N_frec evaluations.
 * 3.  Build the set S_ls with those individuals that potentially may be refined by LS.
 * 4.  Pick the best individual C_LS in S_LS;
 * 5.  If C_LS belongs to an existing LS chain then
 * 6.  	Initialize the LS operator with the LS state stored together with C_LS.
 * 7.  Else
 * 8.  	Initialize the LS operator with the default LS state.
 * 9.  Apply Solis Wets' Algorithm to C_LS with an LS intensity of I_str (Let C^r _LS be the resulting individual).
 * 10. Replace C_LS by C^r _ LS in the steady_state GA population.
 * 11. Store the final LS state along with C^r _LS.
 * 12. If ( not termination-condition ) go to step 2.
 */

/* C++ includes */

#include <iostream>
#include <chrono>
#include <functional>
#include <iomanip>
#include <random>
#include <algorithm>
#include <string>

/* C includes */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <assert.h>

#include <boost/program_options.hpp>

/* My includes */
#include "run_algo.hpp"

namespace po = boost::program_options;

void show_params(
	uint n_runs, uint pop_size,
	uint n_evals, uint n_dimensions,
	std::string FuncObj, std::string output_file,
	bool conv_plot
){
	printf(" | Número de execuções:      %d\n", n_runs);
	printf(" | Tamanho da População:     %d\n", pop_size);
	printf(" | Número de Evals:          %d\n", n_evals);
	printf(" | Número de Dimensões:      %d\n", n_dimensions);
	//printf(" | Fator de Mutação (F):     %.2lf\n", mutation_factor);
	//printf(" | Taxa de Crossover:        %.2lf\n", crossover_rate);
	printf(" | Função Objetivo:          %s\n", FuncObj.c_str());

	if( conv_plot == true ){
		printf(" | Arquivo de Saída          %s\n", output_file.c_str());
	} else {
		printf(" | Gráfico de Convergência:  False\n");
	}
}

int main(int argc, char *argv[]){
	srand(time(NULL));
	uint n_runs, pop_size, n_evals, n_dim;

	bool conv_plot;

	std::string FuncObj, output_file;

	try{
		po::options_description config("Opções");
		config.add_options()
			("runs,r"    , po::value<uint>(&n_runs)->default_value(5)    , "Número de Execuções" )
			("pop_size,p", po::value<uint>(&pop_size)->default_value(100), "Tamanho da População")
			("n_evals,g", po::value<uint>(&n_evals)->default_value(1000), "Número de Avaliações de Função"  )
			("dim,d"     , po::value<uint>(&n_dim)->default_value(10)    , "Número de Dimensões" )
			("func_obj,o", po::value<std::string>(&FuncObj)->default_value("shifted_sphere"), "Função Objetivo"   )
			("conv_plot" , po::value<bool>(&conv_plot)->default_value(false), "Geração de dados para gráficos de convergência")
			("output_file",po::value<std::string>(&output_file)->default_value("output.txt"), "Nome do arquivo de saída para gráficos de convergência" )
			("help,h", "Mostrar texto de Ajuda");

		po::options_description cmdline_options;
		cmdline_options.add(config);
		po::variables_map vm;
		store(po::command_line_parser(argc, argv).options(cmdline_options).run(), vm);
		notify(vm);
		if (vm.count("help")) {
			std::cout << cmdline_options << "\n";
			return 0;
		}
	}catch(std::exception& e){
		std::cout << e.what() << "\n";
		return 1;
	}
	if( FuncObj != "shifted_sphere" && FuncObj != "shifted_rosenbrock"
		&& FuncObj != "shifted_griewank" && FuncObj != "shifted_rastrigin"){

		std::cout << "The algorithm does not support the received method." << std::endl;
		std::cout << "Methods supported: " << std::endl;
		std::cout << " - shifted_sphere\n";
		std::cout << " - shifted_rosenbrock\n";
		std::cout << " - shifted_griewank\n";
		std::cout << " - shifted_rastrigin\n";

		return 1;
	}
	printf(" +==============================================================+ \n");
	printf(" |                          CONFIGURAÇÃO                        |\n");
	printf(" +==============================================================+ \n");
	show_params(n_runs, pop_size, n_evals, n_dim, FuncObj, output_file, conv_plot);

	printf(" +==============================================================+ \n");

	std::vector< std::tuple<double, double, double> > stats;
	std::tuple<double, double, double> run_stats;
	for( int i = 1; i <= n_runs; i++ ){

		run_stats = run_algorithm(pop_size, n_evals, n_dim, FuncObj, output_file, conv_plot);
		stats.push_back(run_stats);

		printf(" | Execução: %-2d Melhor Global: %+.4lf(%+.4lf) Tempo de Execução (s): %.4lf\n",
				i,
				std::get<0>(run_stats),
				std::get<2>(run_stats),
				std::get<1>(run_stats));

	}

	double FO_mean  = 0.0f, FO_std  = 0.0f;
	double T_mean   = 0.0f, T_std   = 0.0f;
	double EFV_mean = 0.0f, EFV_std = 0.0f;
	for( auto it = stats.begin(); it != stats.end(); it++){
		FO_mean += std::get<0>(*it);
		T_mean  += std::get<1>(*it);
		EFV_mean+= std::get<2>(*it);
	}
	FO_mean  /= n_runs;
	T_mean   /= n_runs;
	EFV_mean /= n_runs;

	for( auto it = stats.begin(); it != stats.end(); it++ ){
		FO_std += (( std::get<0>(*it) - FO_mean )*( std::get<0>(*it) - FO_mean ));
		T_std  += (( std::get<1>(*it) - T_mean  )*( std::get<1>(*it) - T_mean  ));
		EFV_std+= (( std::get<2>(*it) - EFV_mean)*( std::get<2>(*it) - EFV_mean));
	}

	FO_std /= n_runs;
	FO_std = sqrt(FO_std);
	T_std  /= n_runs;
	T_std  = sqrt(T_std);
	EFV_std /= n_runs;
	EFV_std = sqrt(EFV_std);
	printf(" +==============================================================+ \n");
	printf(" |                   RESULTADOS DOS EXPERIMENTOS                | \n");
	printf(" +==============================================================+ \n");
	printf(" | Função Objetivo:\n");
	printf(" | \t Média:                 %+.3lf\n", FO_mean);
	printf(" | \t Devio-Padrão:          %+.3lf\n", FO_std);
	printf(" | Distância Euclidiana (EFV)\n");
	printf(" | \t Média:                 %+.3lf\n", EFV_mean);
	printf(" | \t Desvio-Padrão:         %+.3lf\n", EFV_std);
	printf(" | Tempo de Execução: \n");
	printf(" | \t Média:                 %+.3lf\n", T_mean);
	printf(" | \t Devio-Padrão:          %+.3lf\n", T_std);
	printf(" +==============================================================+ \n");
	return 0;
}
