/*
 * Copyright (C) 2013 Miguel Lastra
 *
 * This file is part of GPU-MA-SW-CHAINS.
 *
 * GPU-MA-SW-CHAINS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * GPU-MA-SW-CHAINS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GPU-MA-SW-CHAINS.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "RosenbrockFitness.h"
#include <assert.h>

/*
 * Default value to start is 0;
 */
void evalPopulationRosenbrock(float *individuals,float *fitness,unsigned size,unsigned ndim,float *shift,float fbias, unsigned start, unsigned *order){

  //If this expression evaluates to 0, this causes an assertion failure that terminates the program.
  assert( start == 0 || start < size );

  float zx[size * ndim];
  float Fx = 0.0f;
  float aux;
  float aux2;

  unsigned pos, pos2;

  //calcula o deslocamento para todos;
  for( unsigned indiv = start; indiv < size; indiv++ ){
    for( unsigned dim = 0; dim < ndim; dim++ ){
      pos = computePos(indiv, dim, size, ndim, order);
      zx[pos] = individuals[pos] - shift[dim]/*+1*/;
    }
  }

  //calcula os fitness para cada indiv;
  for( unsigned indiv = start; indiv < size; indiv++ ){
    Fx = 0.0;
    //Fx = Fx + 100*( pow((pow(zx[i],2)-zx[i+1]) , 2) ) + pow((zx[i]-1) , 2);
    for( unsigned dim = 0; dim < (ndim - 1); dim++ ){
      pos  = computePos(indiv, dim, size, ndim, order);
      pos2 = computePos(indiv, dim + 1, size, ndim, order);

      aux  = (zx[pos] * zx[pos]) - zx[pos2];
      aux *= aux;
      aux2 = (zx[pos] - 1);
      aux2 *= aux2;
      Fx += (100*aux) + aux2;
    }

    fitness[indiv] = Fx + fbias;
  }

}

void RosenbrockFitness::eval(Population *population, unsigned start)
{
  unsigned ndim = population->getDimensions();
  unsigned size = population->getSize();

  unsigned *order = NULL;

  evalPopulationRosenbrock(population->getIndividuals(),population->getFitness(),size,ndim,this->shift,this->fbias, start, order);
}

RosenbrockFitness::RosenbrockFitness()
{
  shift = NULL;
}

RosenbrockFitness::RosenbrockFitness(float *_shift, float _fbias, unsigned ndim)
{
  shift = new float[ndim];
  fbias = _fbias;

  this->function = &evalPopulationRosenbrock;

  memcpy(shift, _shift, ndim * sizeof(float));
  //allocateMemoryFunction(&shift,ndim);
  //copyShiftFcuntion(shift,_shift,ndim);
}
