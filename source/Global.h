#ifndef _GLOBAL_H
#define _GLOBAL_H

#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>

typedef struct
{
  float min;
  float max;
  int active;
  float delta;
} domainBounds;

typedef void (*func)(float *individuals,float *fitness,unsigned size,unsigned ndim,float *shift,float fbias, unsigned start, unsigned *order);

unsigned computePos(unsigned indiv,unsigned dim,unsigned size,unsigned ndim,unsigned *order);
unsigned computePosFitness(unsigned indiv, unsigned *order);
unsigned computePosImproved(unsigned indiv,unsigned *order);
unsigned computePosBias(unsigned indiv,unsigned dim,unsigned size,unsigned ndim,unsigned *order);
unsigned computePosDelta(unsigned indiv,unsigned *order);

unsigned findFirstNonImprovedPos(int *improved, unsigned size, unsigned *order);

double distance(float *individuals, unsigned parent, unsigned candidate, unsigned size, unsigned dim, unsigned *order);

void evalPopulationRosenbrock(float *individuals,float *fitness,unsigned size,unsigned ndim,float *shift,float fbias, unsigned start, unsigned *order);
#endif
