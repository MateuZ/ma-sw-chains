/*
 * Copyright (C) 2013 Miguel Lastra
 *
 * This file is part of GPU-MA-SW-CHAINS.
 *
 * GPU-MA-SW-CHAINS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * GPU-MA-SW-CHAINS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GPU-MA-SW-CHAINS.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _FITNESS_H
#define _FITNESS_H

#include "Population.h"
#include "Global.h"
//#include "Global.hcu"
//#include "Fitness.hcu"

class Fitness
{
  protected:
    func function;
    float *shift;
    float fbias;
  public:
    virtual void eval(Population *p, unsigned start = 0) = 0;
    virtual func getFunction();
    float *getShift();
    float getBias();
    ~Fitness();
};


inline float *Fitness::getShift()
{
  return shift;
}

inline float Fitness::getBias()
{
  return fbias;
}

inline Fitness::~Fitness()
{
  //freeMemoryFunction(shift);
}

inline func Fitness::getFunction()
{
  return function;
}

#endif
