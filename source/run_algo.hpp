#ifndef __run_algo__
#define __run_algo__

/* C++ includes */
#include <tuple>
#include <vector>
#include <fstream>
#include <string>
#include <iostream>
#include <iomanip>
#include <sys/time.h>

/* My libs includes */
#include "Individuo.hpp"

typedef std::vector< int > vi;
typedef std::vector< Individual > pop;
typedef std::tuple<double, double, double> tuple_3d;
typedef std::vector< tuple_3d > v_tuple_3d;

/* ------- prototypes ------- */

std::tuple<double, double, double> run_algorithm(
  uint pop_size, uint n_evals, uint n_dim,
  std::string FuncObj, std::string output_file,
  bool conv_plot
);

#endif
