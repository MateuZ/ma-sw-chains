/*
 * Copyright (C) 2013 Miguel Lastra
 *
 * This file is part of GPU-MA-SW-CHAINS.
 *
 * GPU-MA-SW-CHAINS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * GPU-MA-SW-CHAINS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GPU-MA-SW-CHAINS.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Population.h"
//#include "Population.hcu"
#include <iostream>
#include <iomanip>
#include <sys/time.h>
#include <random>
#include <cstdio>

Population::Population()
{
  individuals=NULL;
  step=NULL;
  bias=NULL;
  fitness=NULL;
  improved=NULL;
  numSuccess=NULL;
  numFailed=NULL;
  order=NULL;
  size=0;
  ndim=0;
  minimal=false;
  bounds.active=0;
}

Population::Population(unsigned _size,unsigned _ndim,domainBounds _bounds,bool init)
{
  individuals=NULL;
  step=NULL;
  bias=NULL;
  fitness=NULL;
  improved=NULL;
  numSuccess=NULL;
  numFailed=NULL;
  order=NULL;
  size=_size;
  ndim=_ndim;
  minimal=false;
  bounds=_bounds;

  //allocateMemoryPopulation(&individuals,&step,&bias,&fitness,&improved,&numSuccess,&numFailed,&order,size,ndim);
  unsigned individualsSize = size * ndim;
  individuals = new float[individualsSize];
  bias        = new float[individualsSize];

  step     = new float[size];
  fitness  = new float[size];
  order    = new unsigned[size];

  improved   = new int[size];
  numSuccess = new int[size];
  numFailed  = new int[size];

  if( init ){
    //randomInitPopulation(individuals,step,bias,fitness,improved,numSuccess,numFailed,order,size,ndim,bounds);
    std::mt19937 rng;
    rng.seed(std::random_device{}());
    std::uniform_real_distribution<double> d(0.00, 1.00);

    float range;
    if( bounds.active ){
      range = bounds.max - bounds.min;
    } else {
      range = 1.0;
      bounds.min = 0.0;
    }

    float randomNumber;
    unsigned pos;
    for( int indiv = 0; indiv < size; indiv++ ){
      order[indiv]      = indiv;
      fitness[indiv]    = 1e30;
      step[indiv]       = bounds.delta;
      improved[indiv]   = 0;
      numSuccess[indiv] = 0;
      numFailed[indiv]  = 0;
      for( int dim = 0; dim < ndim; dim++ ){
        pos = computePos(indiv, dim, size, ndim, NULL);
        individuals[pos] = (d(rng) * range) + bounds.min;
        bias[pos] = 0.0;
      }
    }
  }
}

void Population::minimalInit(unsigned _size,unsigned _ndim){
  individuals = NULL;
  step        = NULL;
  bias        = NULL;
  fitness     = NULL;
  improved    = NULL;
  numSuccess  = NULL;
  numFailed   = NULL;
  order       = NULL;
  size        = _size;
  ndim        = _ndim;
  minimal     = true;

  //allocateMinimalMemory(&individuals,&fitness,size,ndim);
  unsigned individualsSize = size * ndim;

  individuals = new float[individualsSize];
  fitness     = new float[size];
}

void Population::sortByFitness()
{
  std::sort(order, order + size, [&](unsigned i1, unsigned i2) { return fitness[i1] < fitness[i2]; });
  //sortPopulationByFitness(individuals,fitness,order,size,ndim);
  /*
  std::cout << "Order obtained: " << std::endl;
  for(unsigned i = 0; i < size; ++i)
    std::cout << order[i] << " ~ " << fitness[order[i]] <<  std::endl;
  */
}

void Population::print(int verbose)
{
  //printf("WARNING: NOT CONSIDERING THE FITNESS ORDER.\n");
  //printf("computePosFitness()\n");
  //printFitness(individuals,fitness,order,size,ndim,verbose);
  unsigned pos;
  for( int indiv = 0; indiv < size; indiv++ ){
    for( int dim = 0; dim < ndim; dim++ ){
      pos = computePos(indiv, dim, size, ndim, order);
      printf("%.3f ", individuals[pos]);
    }
    printf(" [ %.3f ] \n", fitness[computePosFitness(indiv, order)]);
  }
}

void Population::calcDistance(unsigned parent, unsigned candidate){
  printf("Calculando distância entre: %u e %u\n", parent, candidate);
  printf("Distância: %.4lf\n", distance(individuals, 0, 1, size, ndim, order));
}

Population::~Population()
{
  //freeMemoryPopulation(individuals,step,bias,fitness,improved,numSuccess,numFailed,order,minimal);
  if( fitness != NULL )
    free(fitness);

  if( individuals != NULL )
    free( individuals );

  if( not minimal ){
    if( step != NULL )
      free( step );

    if( bias != NULL )
      free( bias );

    if( improved != NULL )
      free( improved );

    if( numSuccess != NULL )
      free( numSuccess );

    if( numFailed != NULL )
      free( numFailed );

    if( order != NULL )
      free( order );
    }
}

double Population::distanceToOptimum(float *shift)
{
  unsigned pos;
  double var = 0.0, accum = 0.0;
  for( int dim = 0; dim < ndim; dim++ ){
    //0 contains the best individual
    pos = computePos(0, dim, size, ndim, order);
    var = individuals[pos] - shift[dim];
    accum += (var * var);
  }
  return sqrt(accum);
}

float Population::getBestFitness()
{
  //printf("[%d] ", order[0]);
  return fitness[ order[0] ];
}

float Population::getBestIndividualStatistics(float &variance)
{
  //return computeIndividualStatistics(individuals,size,ndim,order,variance);
}

double Population::calcInertia(){

  std::vector<double> c_i(ndim, 0.0);

  //definindo os centroídes de cada dimensão;
	for( unsigned i = 0; i < ndim; i++ ){
		for( unsigned j = 0; j < size; j++ ){
			unsigned pos = computePos(j, i, size, ndim, order);
			c_i[i] += (individuals[pos] / size);
		}
	}

	double Isd = 0.0f;
	double temp = 0.0f;
	unsigned pos;

	for( unsigned i = 0; i < ndim; i++ ){
		temp = 0.0f;
		for( unsigned j = 0; j < size; j++ ){
			pos = computePos(j, i, size, ndim, order);
			temp += ( ((individuals[pos] - c_i[i]) * (individuals[pos] - c_i[i])) / (size - 1));
		}
		Isd += sqrt(temp);
	}

	Isd = Isd/ndim;

  return Isd;
}
